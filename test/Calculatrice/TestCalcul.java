package Calculatrice;

import org.junit.Test;
import static org.junit.Assert.*;

public class TestCalcul {

    @Test
    public void empiler(){
        MoteurRPN test = new MoteurRPN();
        test.empiler(10);
        assertTrue("Reussi",test.depiler() == 10);
    }

    @Test
    public void depiler(){
        MoteurRPN test = new MoteurRPN();
        test.empiler(10);
        test.depiler();
        assertTrue("Reussi",test.liste.isEmpty());
    }

    @Test
    public void eval(){
        MoteurRPN test = new MoteurRPN();
        try {
            test.enregistrer(10);
        } catch (PilePleineException e) {
            e.printStackTrace();
        }
        try {
            test.enregistrer(5);
        } catch (PilePleineException e) {
            e.printStackTrace();
        }
        try {
            test.calculer(Operation.DIV);
        } catch (PileVideException e) {
            e.printStackTrace();
        } catch (PilePleineException e) {
            e.printStackTrace();
        } catch (DivisionPar0 divisionPar0) {
            divisionPar0.printStackTrace();
        }
        assertTrue("Reussi", test.depiler() == 2);
    }
    @Test
    public void evalP() throws PilePleineException, PileVideException, DivisionPar0 {
        MoteurRPN test = new MoteurRPN();
        try {
            test.enregistrer(5);
        } catch (PilePleineException e) {
            e.printStackTrace();
        }
        try {
            test.enregistrer(10);
        } catch (PilePleineException e) {
            e.printStackTrace();
        }
        test.calculer(Operation.PLUS);
        assertTrue("Reussi", test.depiler() == 15);
    }

    @Test
    public void evalM() throws PilePleineException, PileVideException, DivisionPar0 {
        MoteurRPN test = new MoteurRPN();
        try {
            test.enregistrer(10);
        } catch (PilePleineException e) {
            e.printStackTrace();
        }
        try {
            test.enregistrer(5);
        } catch (PilePleineException e) {
            e.printStackTrace();
        }
        test.calculer(Operation.MOINS);
        assertTrue("Reussi", test.depiler() == -5);
    }
}
