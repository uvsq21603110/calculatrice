package Calculatrice;

public class PileVideException extends PileException {

    public PileVideException(){
        super("Pile vide");
    }
}
