package Calculatrice;


import java.util.ArrayList;
import java.util.List;


public class MoteurRPN {

    public final static int Max_Value = 99999999;
    public final static int Min_Value = 0;

    public MoteurRPN() {}

    ArrayList<Double> liste = new ArrayList<>();

    public void enregistrer(double op1) throws PilePleineException {
        if ( Math.abs(op1) < Max_Value && Math.abs(op1) >= Min_Value)
            this.empiler(op1);
        else System.out.println("non ajoute");

    }

    public void calculer(Operation operation) throws PileVideException, PilePleineException, DivisionPar0 {
        double res = 0;
        if (this.liste.size() > 1){
                if (operation.getSymbole() == '+'){

                    res = operation.eval(this.depiler(), this.depiler());
                    System.out.println("res = " + res);
                    this.empiler(res);
                }else if (operation.getSymbole() == '-'){

                    res = operation.eval(this.depiler(), this.depiler());
                    System.out.println("res = " + res);
                    this.empiler(res);
                }else if (operation.getSymbole() == '*'){

                    res = operation.eval(this.depiler(), this.depiler());
                    System.out.println("res = " + res);
                    this.empiler(res);
                }else if (operation.getSymbole() == '/'){

                    res = operation.eval(this.depiler(), this.depiler());
                    System.out.println("res = " + res);
                    this.empiler(res);
                }else ;
            } else throw new PileVideException();
    }

    public List<Double> retourner() {
        return this.liste;
    }

    public void afficher(){
        String s = new String();
        for (double l: liste){
            s += l;
            s += " ";
        }
        System.out.println(s);
    }

    public void empiler(double n){
        if (this.liste.size() < 100) {
            this.liste.add(n);
            this.afficher();
        }
        else try {
            throw new PilePleineException();
        } catch (PilePleineException e) {
            e.printStackTrace();
        }
    }

    public double depiler(){
        if (this.liste.size() > 0){
            double nbr = ((Double)this.liste.get(this.liste.size() -1)).doubleValue();
            this.liste.remove(this.liste.size() - 1);
            return nbr;
        } else {
            try {
                throw new PileVideException();
            } catch (PileVideException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }
}