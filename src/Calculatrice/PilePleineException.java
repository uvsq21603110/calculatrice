package Calculatrice;

public class PilePleineException extends PileException {

    public PilePleineException(){
        super("Pile Pleine");
    }
}
