package Calculatrice;

import java.util.Scanner;

import static java.lang.System.exit;

public class SaisieRPN{

    private Scanner scan;
    private MoteurRPN mrnp;

    public SaisieRPN(MoteurRPN mrnp){
        this.scan = new Scanner(System.in);
        this.mrnp = mrnp;
    }

    public void Saisie() {

        String entree = new String();
        entree = this.scan.next();

        if (entree.equals("exit")){
            exit(0);
        }
        else if (entree.equals("+") ) {
            try {
                mrnp.calculer(Operation.PLUS);
            } catch (PileVideException e) {
                e.printStackTrace();
            } catch (PilePleineException e) {
                e.printStackTrace();
            } catch (DivisionPar0 divisionPar0) {
                divisionPar0.printStackTrace();
            }
        }
        else if (entree.equals("-") ) {
            try {
                mrnp.calculer(Operation.MOINS);
            } catch (PileVideException e) {
                e.printStackTrace();
            } catch (PilePleineException e) {
                e.printStackTrace();
            } catch (DivisionPar0 divisionPar0) {
                divisionPar0.printStackTrace();
            }
        }
        else if (entree.equals("*") ) {
            try {
                mrnp.calculer(Operation.MULT);
            } catch (PileVideException e) {
                e.printStackTrace();
            } catch (PilePleineException e) {
                e.printStackTrace();
            } catch (DivisionPar0 divisionPar0) {
                divisionPar0.printStackTrace();
            }
        }
        else if (entree.equals("/") ) {
            try {
                mrnp.calculer(Operation.DIV);
            } catch (PileVideException e) {
                e.printStackTrace();
            } catch (PilePleineException e) {
                e.printStackTrace();
            } catch (DivisionPar0 divisionPar0) {
                divisionPar0.printStackTrace();
            }
        }
        else if (entree.matches("\\p{Digit}+") || entree.matches("-\\p{Digit}+") || entree.matches("\\p{Digit}+.\\p{Digit}") || entree.matches("-\\p{Digit}+.\\p{Digit}+")){
            try {
                mrnp.enregistrer(Double.parseDouble(entree));
            } catch (PilePleineException e) {
                e.printStackTrace();
            }
        }
        else {
            System.out.println("Vous n'avez pas rentre un entier ou un operateur");
        }
    }
}